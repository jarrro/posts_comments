
var HOME = (function() {   

    const Home = { 
        props: ['posts'],
        template: `
            <div>
                <div class="row">
                    <div class="col-md-6">
                        <h1>Posts</h1>
                    </div>
                    <div class="col-md-6 button-right">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addPost">
                            Add Post
                        </button>
                    </div>
                </div>
                <home
                    is="home" 
                    :posts="posts">
                </home>
            </div>
            `,
        methods: {
            addPost: function(p) {
                this.$http({
                    url: 'https://jsonplaceholder.typicode.com/posts/'+this.$route.params.id+'/comments',
                    method: 'POST',
                    params: {
                        title: p.title,
                        body: p.body,
                        userId: 1
                    }
                }).then(response => {
                    console.log(response);
                });
            }
        }
    }

    return Home;
})();