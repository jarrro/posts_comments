(function() {
    'use strict';

    const router = new VueRouter({
        routes: [
            { 
                path: '/', 
                components: {
                    default: HOME
                }
            },
            { path: '/post/:id', component: POST }
        ]
    })    

    const app = new Vue({
        router,
        data: {
            posts: {},
            user_id: '',
            id: '',
            title: '',
            body: ''
        },
        mounted() {
            this.$http.get('https://jsonplaceholder.typicode.com/posts')
                .then(response => {
                    this.posts = response.body;
                });
        },
        components: {
            posts: POSTS,
            comments: COMMENTS
        }
    }).$mount('#app')

})(HOME, POSTS, POST, COMMENTS);
