var POST = (function() {
    const Post = { 
        template: `
            <div>
                <h1>Post #{{ $route.params.id }}</h1>
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">
                            {{post.title}}
                        </h4>
                        <p class="card-text">
                            {{post.body}}
                        </p>
                    </div>
                </div>
                <hr>
                <h2>Add comment</h2>
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" placeholder="Enter name" v-model="c.name">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" placeholder="Enter email" v-model="c.email">
                        </div>
                        <div class="form-group">
                            <label for="text">Comment</label>
                            <textarea class="form-control" id="text" rows="3" v-model="c.text"></textarea>
                        </div>
                        <button class="btn btn-primary" v-on:click="addComment(c)">Add comment</button>
                    </div>
                </div>
                <hr>
                <h2>Comments</h2>
                <comments is="comments" :comments="comments"></comments>
            </div>`,
        data() {
            return {
                post: '',
                comments: '',
                c: {
                    name: '',
                    email: '',
                    text: ''
                }
            }
        },
        methods: {
            addComment: function(c) {
                this.$http.post('https://jsonplaceholder.typicode.com/posts/'+this.$route.params.id+'/comments')
                .then(response => {
                    console.log(response);
                });
            }
        },
        mounted() {
            this.$http.get('https://jsonplaceholder.typicode.com/posts/'+this.$route.params.id)
            .then(response => {
                this.post = response.body;
            });

            this.$http.get('https://jsonplaceholder.typicode.com/posts/'+this.$route.params.id+'/comments')
                .then(response => {
                    this.comments = response.body;
                });
        }
    }

    return Post;
})(); 