var COMMENTS = (function() {
    const Comments = Vue.component('comments', {
        props: ['comments'],
        template: 
            `
            <div>
                <div class="card" v-for="comment in comments">
                    <div class="card-body">
                        <h4 class="card-title">
                            {{comment.name}}
                        </h4>
                        <h6 class="card-subtitle mb-2 text-muted">
                            {{comment.email}}
                        </h6>
                        <p class="card-text">
                            {{comment.body}}
                        </p>
                    </div>
                </div>
            </div>
            `
    });

    return Comments;
})();