var POSTS = (function() {
    var posts = Vue.component('home', {    
        props: ['posts'],    
        template: `
            <div>
                <div class="card" v-for="post in posts">
                    <div class="card-body">
                        <h4 class="card-title">
                            {{post.title}}
                        </h4>
                        <p class="card-text">
                            {{post.body}}
                        </p>
                        <router-link :to="'/post/'+post.id">See more..</router-link>
                    </div>
                </div>
            </div>
        `,
    });

    return posts
})();